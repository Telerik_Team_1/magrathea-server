import { Funds } from './funds.entity';
import { Settings } from './settings.entity';
import { Order } from './order.entity';
import { Watchlist } from './watchlist.entity';
import { Role } from './role.entity';
import {
  Column,
  PrimaryGeneratedColumn,
  Entity,
  ManyToOne,
  OneToMany,
  OneToOne,
  JoinColumn,
  JoinTable,
  ManyToMany,
} from 'typeorm';
import { Company } from './company.entity';

@Entity({
  name: 'users',
})
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(type => Role, role => role.users, { eager: true })
  role: Role;

  @ManyToOne(type => User, user => user.clients)
  manager: Promise<User>;

  @OneToMany(type => User, user => user.manager)
  clients: Promise<User[]>;

  @ManyToMany(type => Company, company => company.users)
  @JoinTable()
  companies: Promise<Company[]>;

  @OneToOne(type => Settings, settings => settings.user, { eager: true })
  @JoinColumn()
  settings: Settings;

  @OneToOne(type => Funds, funds => funds.client, { eager: true})
  @JoinColumn()
  funds: Funds;

  @Column({ default: '' })
  fullname: string;

  @Column()
  dateregistered: Date;

  @Column({ unique: true })
  email: string;

  @Column({ default: '' })
  password: string;

  @OneToMany(type => Order, order => order.client)
  orders: Promise<Order[]>;
}
