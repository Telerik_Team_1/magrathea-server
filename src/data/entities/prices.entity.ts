import { Company } from './company.entity';
import { Column, PrimaryGeneratedColumn, Entity, ManyToOne } from 'typeorm';

@Entity({
  name: 'prices',
})
export class Price {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  opendate: Date;

  @Column({ type: 'double' })
  startprice: number;

  @Column({ type: 'double' })
  endprice: number;

  @Column({ type: 'double' })
  highprice: number;

  @Column({ type: 'double' })
  lowprice: number;

  @ManyToOne(type => Company, company => company.prices, { eager: true })
  company: Company;

}