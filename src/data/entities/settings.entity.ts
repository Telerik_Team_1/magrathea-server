import { User } from './user.entity';
import { PrimaryGeneratedColumn, Entity, OneToOne, Column } from 'typeorm';

@Entity({
  name: 'settings',
})
export class Settings {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable: true })
  dob: Date;

  @Column({ default: '' })
  address: string;

  @OneToOne(type => User, user => user.settings)
  user: Promise<User>;
}
