import { User } from 'src/data/entities/user.entity';

export class ClientRecord {
  manager: string;
  clients: User[];
}