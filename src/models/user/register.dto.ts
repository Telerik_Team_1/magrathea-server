import { IsString, Matches, IsEmail } from 'class-validator';
import { Funds } from 'src/data/entities/funds.entity';
import { GetUserDTO } from './get-user.dto';

export class RegisterDTO {
    @IsString()
    name: string;

    @IsString()
    @Matches(/(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}/)
    password: string;

    @IsString()
    email: string;

    dob: Date;
    address: string;

    @IsString()
    role: string;

    @IsString()
    @IsEmail()
    manager: GetUserDTO;

    funds: Funds;
}