import { User } from 'src/data/entities/user.entity';
import { IsEmail } from 'class-validator';
import { Funds } from 'src/data/entities/funds.entity';

export class UserUpdateDTO {
    fullname: string;
    email: string;
    dob: Date;
    address: string;
    manager: {name: string, email: string};
    role: string;
    funds?: Funds;
    id: string;
}