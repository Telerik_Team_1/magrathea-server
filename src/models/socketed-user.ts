import { GetUserDTO } from './user/get-user.dto';
import * as WebSocket from 'ws';

export class SocketedUser {
  socket: WebSocket;
  user: GetUserDTO;
  token: string;
}