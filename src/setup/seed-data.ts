import 'reflect-metadata';
import { createConnection } from 'typeorm';
import { Role } from '../data/entities/role.entity';
import { Status } from '../data/entities/status.entity';
import { Company } from '../data/entities/company.entity';
import { Industry } from '../data/entities/industry.entity';
import { User } from '../data/entities/user.entity';
import { Settings } from '../data/entities/settings.entity';
import * as bcrypt from 'bcrypt';
import { Funds } from '../data/entities/funds.entity';
import { Order } from '../data/entities/order.entity';

// tslint:disable:no-console

createConnection().then(async (conn) => {

  let admin, manager, client;

  let _client = null;
  let _company = null;
  let _opened = null;

  // Setup user roles
  const roleRepo = conn.getRepository<Role>(Role);
  const fundsRepo = conn.getRepository<Funds>(Funds);
  const orderRepo = conn.getRepository<Order>(Order);

  try {
    const roleAdmin = new Role();
    roleAdmin.rolename = 'admin';
    admin = await roleRepo.save(roleAdmin);

    const roleManager = new Role();
    roleManager.rolename = 'manager';
    manager = await roleRepo.save(roleManager);

    const roleClient = new Role();
    roleClient.rolename = 'client';
    client = await roleRepo.save(roleClient);

    const roleClosed = new Role();
    roleClosed.rolename = 'closed';
    await roleRepo.save(roleClosed);

    console.log(`User roles written successfully.`);
  } catch (e) {
    console.warn('Skipping user roles...');
  }

  // Setup the default admin
  // email: admin@test.com
  // password: TestPassword1!

  // Setup the default manager
  // email: manager@test.com
  // password: TestPassword1!

  const userRepo = conn.getRepository<User>(User);
  const settingsRepo = conn.getRepository<Settings>(Settings);

  try {
    const settings1 = new Settings();
    settings1.address = 'testAddress';
    settings1.dob = new Date();

    const user1 = new User();
    user1.fullname = 'Athur Dent';
    user1.email = 'admin@test.com';
    user1.dateregistered = new Date();
    user1.settings = await settingsRepo.save(settings1);
    user1.role = admin;
    user1.password = await bcrypt.hash('TestPassword1!', 10);

    await userRepo.save(user1);

    const settings2 = new Settings();
    settings2.address = 'testAddress';
    settings2.dob = new Date();

    const user2 = new User();
    user2.fullname = 'Zaphod Beeblebrox';
    user2.email = 'manager@test.com';
    user2.dateregistered = new Date();
    user2.settings = await settingsRepo.save(settings2);
    user2.role = manager;
    user2.password = await bcrypt.hash('TestPassword1!', 10);

    const manager_user = await userRepo.save(user2);

    const _funds = new Funds();
    _funds.currentamount = 100000.00;
    const funds = await fundsRepo.save(_funds);

    const settings3 = new Settings();
    settings3.address = 'testAddress';
    settings3.dob = new Date();

    const user3 = new User();
    user3.fullname = 'Ford Prefect';
    user3.email = 'client@test.com';
    user3.dateregistered = new Date();
    user3.settings = await settingsRepo.save(settings3);
    user3.role = client;
    user3.funds = funds;
    user3.manager = Promise.resolve(manager_user);
    user3.password = '-';

    _client = await userRepo.save(user3);

    console.log(`Default admin and client written successfully.`);
  } catch (e) {
    console.log(e);
    console.warn('Skipping admin creation...');
  }

  // Setup orders status
  const statusRepo = conn.getRepository<Status>(Status);

  try {
    const statusOpened = new Status();
    statusOpened.statusname = 'opened';
    _opened = await statusRepo.save(statusOpened);

    const statusClosed = new Status();
    statusClosed.statusname = 'closed';
    await statusRepo.save(statusClosed);

    console.log(`Status table written successfully.`);
  } catch (e) {
    console.warn('Skipping orders status...');
  }

  // Setup mock-up industries

  let _industry1, _industry2, _industry3, _industry4, _industry5;

  const industryRepo = conn.getRepository<Industry>(Industry);
  try {
    const industry1 = new Industry();
    industry1.name = 'Mindblowers and Planetbusters';
    _industry1 = await industryRepo.save(industry1);

    const industry2 = new Industry();
    industry2.name = 'Ultimate Evil';
    _industry2 = await industryRepo.save(industry2);

    const industry3 = new Industry();
    industry3.name = 'More Than Energy';
    _industry3 = await industryRepo.save(industry3);

    const industry4 = new Industry();
    industry4.name = 'Super Superfood';
    _industry4 = await industryRepo.save(industry4);

    const industry5 = new Industry();
    industry5.name = 'Absolute Evil';
    _industry5 = await industryRepo.save(industry5);

    console.log(`Test industries written`);
  } catch (e) {
    console.warn('Skipping test industries...');
  }

  // Setup mock-up markets
  const companyRepo = conn.getRepository<Company>(Company);

  try {
    const industries = await industryRepo.find({});
    const firstIndustry = industries[0];

    const company1 = new Company();
    company1.name = 'Gargleblasters';
    company1.industry = _industry1;
    company1.abbr = 'PGB';
    company1.address = 'Santraginus V';
    company1.ceo = 'Zaphod Beeblebrox';
    company1.icon = 'gargleblaster.png';
    company1.closedate = new Date(2000, 0, 0, 12, 0);
    _company = await companyRepo.save(company1);

    const company2 = new Company();
    company2.name = 'Tea (almost like)';
    company2.industry = _industry4;
    company2.abbr = 'TT';
    company2.address = 'Heart of Gold';
    company2.ceo = '---';
    company2.icon = 'tea.png';
    company2.closedate = new Date(2000, 0, 0, 8, 0);
    await companyRepo.save(company2);

    const company3 = new Company();
    company3.name = 'Spice (Melange)';
    company3.industry = _industry4;
    company3.abbr = 'SPC';
    company3.address = 'Arakis, Dune';
    company3.ceo = '---';
    company3.icon = 'spice.png';
    company3.closedate = new Date(2000, 0, 0, 8, 0);
    await companyRepo.save(company3);

    const company4 = new Company();
    company4.name = 'Omega Molecules';
    company4.industry = _industry1;
    company4.abbr = '[ Ω ]';
    company4.address = 'Borg Space';
    company4.ceo = '---';
    company4.icon = 'omega.png';
    company4.closedate = new Date(2000, 0, 0, 8, 0);
    await companyRepo.save(company4);

    const company5 = new Company();
    company5.name = 'Naquadah';
    company5.industry = _industry3;
    company5.abbr = 'NQD';
    company5.address = 'Abydos';
    company5.ceo = '---';
    company5.icon = 'naquadah.png';
    company5.closedate = new Date(2000, 0, 0, 8, 0);
    await companyRepo.save(company5);

    const company6 = new Company();
    company6.name = 'Kyber Crystal';
    company6.industry = _industry3;
    company6.abbr = 'KRC';
    company6.address = 'Ilum';
    company6.ceo = '---';
    company6.icon = 'crystal.png';
    company6.closedate = new Date(2000, 0, 0, 8, 0);
    await companyRepo.save(company6);

    const company7 = new Company();
    company7.name = 'Wonka Industries';
    company7.industry = _industry4;
    company7.abbr = '-W-';
    company7.address = 'Candycane Street. 23';
    company7.ceo = '---';
    company7.icon = 'wonka.png';
    company7.closedate = new Date(2000, 0, 0, 8, 0);
    await companyRepo.save(company7);

    const company8 = new Company();
    company8.name = 'Acme Corp.';
    company8.industry = _industry5;
    company8.abbr = 'ACME';
    company8.address = 'Looney Tunes';
    company8.ceo = '---';
    company8.icon = 'acme.png';
    company8.closedate = new Date(2000, 0, 0, 8, 0);
    await companyRepo.save(company8);

    const company9 = new Company();
    company9.name = 'zOrg Industries';
    company9.industry = _industry5;
    company9.abbr = 'zOrg';
    company9.address = 'Earth, 22nd century';
    company9.ceo = '---';
    company9.icon = 'zorg.png';
    company9.closedate = new Date(2000, 0, 0, 8, 0);
    await companyRepo.save(company9);

    const company10 = new Company();
    company10.name = 'Cyberdyne Systems';
    company10.industry = _industry2;
    company10.abbr = 'CS(S)';
    company10.address = '18144 El Camino Real, Sunnyvale, California';
    company10.ceo = '---';
    company10.icon = 'skynet.png';
    company10.closedate = new Date(2000, 0, 0, 8, 0);
    await companyRepo.save(company10);

    const company11 = new Company();
    company11.name = 'Soylent';
    company11.industry = _industry4;
    company11.abbr = '|S|';
    company11.address = 'Dystopian Earth';
    company11.ceo = '---';
    company11.icon = 'soylent.png';
    company11.closedate = new Date(2000, 0, 0, 8, 0);
    await companyRepo.save(company11);

    const company12 = new Company();
    company12.name = 'Weyland-Yutani';
    company12.industry = _industry2;
    company12.abbr = 'WY';
    company12.address = 'Tokyo | San Francisco';
    company12.ceo = '---';
    company12.icon = 'weylandyutani.png';
    company12.closedate = new Date(2000, 0, 0, 8, 0);
    await companyRepo.save(company12);

    console.log(`Test markets written`);
  } catch (e) {
    console.log(e);
    console.warn('Skipping test markets...');
  }

  try {
    const order = new Order();
    order.buyprice = 60;
    order.client = Promise.resolve(_client);
    order.opendate = new Date();
    order.units = 2;
    order.company = _company;
    order.status = _opened;
    await orderRepo.save(order);
  } catch (e) {
    console.log(e);
  }

  conn.close();
});