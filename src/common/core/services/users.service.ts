import { ClientRegisterDTO } from './../../../models/user/client-register.dto';
import { GetUserDTO } from '../../../models/user/get-user.dto';
import { UserLoginDTO } from '../../../models/user/user-login.dto';
import { Injectable, BadRequestException } from '@nestjs/common';
import { Repository, AdvancedConsoleLogger } from 'typeorm';
import { User } from '../../../data/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';

import * as bcrypt from 'bcrypt';
import { JwtPayload } from '../../../interfaces/jwt-payload';
import { Role } from '../../../data/entities/role.entity';
import { Funds } from '../../../data/entities/funds.entity';
import { RegisterDTO } from '../../../models/user/register.dto';
import { Settings } from 'src/data/entities/settings.entity';
import { UserUpdateDTO } from 'src/models/user/user-update.dto';
import { CachingService } from './caching.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    @InjectRepository(Role)
    private readonly roleRepository: Repository<Role>,
    @InjectRepository(Funds)
    private readonly fundsRepository: Repository<Funds>,
    @InjectRepository(Settings)
    private readonly settingsRepository: Repository<Settings>,
    private readonly cache: CachingService,
  ) { }

  async createUser(userData: Partial<RegisterDTO>): Promise<User>{
    const foundUser = await this.usersRepository.findOne({ email: userData.email });
    if (foundUser) {
      throw new BadRequestException('Email already exist');
    }
    try {
      const user = new User();
      const settings = new Settings();
      settings.address = userData.address;
      settings.dob = userData.dob;

      user.fullname = userData.name;
      user.email = userData.email;
      user.dateregistered = new Date();
      user.settings = await this.settingsRepository.save(settings);
      user.role = await this.roleRepository.findOne({ rolename: userData.role });
      if (userData.role === 'admin' || userData.role === 'manager') {
        user.password = await bcrypt.hash(userData.password, 10);
      }
      if (userData.role === 'client') {
        const foundManager = await this.usersRepository.findOne({ email: userData.manager.email });
        if (!foundManager) {
          throw new BadRequestException('Manager does not exist');
        }
        user.manager = Promise.resolve(foundManager);
        const funds = new Funds();
        funds.currentamount = +userData.funds;
        await this.fundsRepository.save(funds);
        user.funds = funds;
      }

      await this.usersRepository.create(user);
      try{
        const created =  await this.usersRepository.save(user);
        this.cache.addUser(created);
        return created;
      } catch (err) {
        console.error(err);
      }
    } catch (error) {
      throw new BadRequestException();
    }
  }
  async updateUser(userData: Partial<RegisterDTO>, id: string): Promise<User>{
    const foundUser = await this.usersRepository.findOne({ id });
    if (!foundUser) {
      throw new BadRequestException('User does not already exist');
    }
    try {
      foundUser.fullname = userData.name;
      foundUser.email = userData.email;

      foundUser.settings.dob = new Date(userData.dob);
      foundUser.settings.address = userData.address;
      await this.settingsRepository.save(foundUser.settings);

      if (foundUser.role.rolename === 'admin' || foundUser.role.rolename === 'manager') {
        foundUser.password = await bcrypt.hash(userData.password, 10);
      }

      if (foundUser.role.rolename === 'client') {
        if (!userData.manager.email) {
          throw new BadRequestException('Manager does not exist');
        }
        const foundManager = await this.usersRepository.findOne({ email: userData.manager.email });
        if (!foundManager) {
          throw new BadRequestException('Manager does not exist');
        }
        foundUser.manager = Promise.resolve(foundManager);
      }

      const updated =  await this.usersRepository.save(foundUser);
      this.cache.updateUser(updated);
      return updated;

    } catch (error) {
      throw new BadRequestException();
    }
  }
  async getUserById(id: string): Promise<UserUpdateDTO> {
    try {
      const user = await this.usersRepository.findOne({ id });
      const userData = new UserUpdateDTO();

      if (user.role.rolename === 'client') {
        const manager = await user.manager;
        userData.manager = {
          name: manager.fullname,
          email: manager.email,
        };
      }

      userData.address = await user.settings.address;
      userData.dob = user.settings.dob;
      userData.role = user.role.rolename;
      userData.email = user.email;
      userData.fullname = user.fullname;
      userData.id = user.id;
      userData.funds = user.funds;

      return userData;
    } catch (error) {
      throw new BadRequestException('No such user');
    }
  }

  // ==> Only admin can register new client and managers profiles
  async createManager(manager: RegisterDTO): Promise<User> {
    const foundManager = await this.usersRepository.findOne({ email: manager.email });
    if (foundManager) {
      throw new BadRequestException('Email already exist');
    }

    try {
      const managerRole = await this.roleRepository.findOne({ rolename: 'manager' });
      const newManager = await this.usersRepository.create();

      newManager.fullname = manager.name;
      newManager.email = manager.email;
      newManager.password = manager.password = await bcrypt.hash(manager.password, 10);
      newManager.dateregistered = new Date();
      newManager.role = managerRole;

      return await this.usersRepository.save(newManager);
    } catch (error) {
      throw new BadRequestException();
    }
  }

  async createAdmin(admin: RegisterDTO): Promise<User> {
    const foundAdmin = await this.usersRepository.findOne({ email: admin.email });
    if (foundAdmin) {
      throw new BadRequestException('Email already exist');
    }
    try {
      const role = await this.roleRepository.findOne({ rolename: 'admin' });
      const newAdmin = await this.usersRepository.create();
      newAdmin.fullname = admin.name;
      newAdmin.email = admin.email;
      newAdmin.dateregistered = new Date();
      newAdmin.role = role;
      newAdmin.password = admin.password = await bcrypt.hash(admin.password, 10);
      return await this.usersRepository.save(newAdmin);

    } catch (error) {
      throw new BadRequestException();
    }
  }

  // async createClient(managerId: string, client: ClientRegisterDTO): Promise<User> {
  //   const foundClient = await this.usersRepository.findOne({ email: client.email });
  //   if (foundClient) {
  //     throw new BadRequestException('Email already exist');
  //   }

  //   try {
  //     const role = await this.roleRepository.findOne({ rolename: 'client' });
  //     const manager = await this.usersRepository.findOne({ id: managerId });

  //     const funds = await this.fundsRepository.create();
  //     funds.currentamount = +client.amount;
  //     await this.fundsRepository.save(funds);

  //     const newClient = await this.usersRepository.create();
  //     newClient.fullname = client.fullname;
  //     newClient.email = client.email;
  //     newClient.dateregistered = new Date();
  //     newClient.role = role;
  //     newClient.manager = manager;
  //     newClient.funds = funds;

  //     return await this.usersRepository.save(newClient);
  //   } catch (error) {
  //     throw new BadRequestException('Client cannot be created');
  //   }
  // }

  async validateUser(payload: JwtPayload): Promise<GetUserDTO> {
    const userFound: any = await this.usersRepository.findOne({ where: { email: payload.email } });
    return userFound;
  }

  async signIn(user: UserLoginDTO): Promise<User> {
    const userFound: User = await this.usersRepository.findOne({ where: { email: user.email } });

    if (userFound) {
      const result = await bcrypt.compare(user.password, userFound.password);
      if (result) {
        return userFound;
      }
    }

    return null;
  }

  async getAll() {
    return this.usersRepository.find({});

  }
  async getAllManagers() {
    const managerRole = await this.roleRepository.findOne({rolename: 'manager'});
    const managers =  await this.usersRepository.find({where: {role: managerRole}});
    return managers;
  }

  async getUser(id: string): Promise<User> {
    try {
      const user = await this.usersRepository.findOne({ id });
      return user;
    } catch (error) {
      throw new BadRequestException('No such user');
    }
  }

  async getManager(id: string): Promise<User> {
    try {
      const manager = await this.usersRepository.findOne({ id });
      return manager;
    } catch (error) {
      throw new BadRequestException('No such manager');
    }
  }

  // Need info on settings and how will it work
  async getUserSettings(id: string) {
    const user: User = await this.usersRepository.findOne({ id });
    return user.settings;
  }

  async updateUserSettings(id: string, settings: any) {
    const user: User = await this.usersRepository.findOne({ id });
    user.settings = settings;
  }
}
