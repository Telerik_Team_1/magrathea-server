import { Status } from './../../../data/entities/status.entity';
import { Company } from './../../../data/entities/company.entity';
import { Injectable, HttpException, HttpStatus, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/data/entities/user.entity';
import { Repository } from 'typeorm';
import { Order } from '../../../data/entities/order.entity';
import { OrderDTO } from '../../../models/order/order.dto';
import { Price } from 'src/data/entities/prices.entity';
import { UpdaterService } from 'src/sockets/services/updater.service';
import { Funds } from 'src/data/entities/funds.entity';
import { default_type } from 'mime';
import { CachingService } from './caching.service';
import { runInThisContext } from 'vm';

@Injectable()
export class OrderService {
    constructor(
        @InjectRepository(Order)
        private readonly orderRepository: Repository<Order>,
        @InjectRepository(User)
        private readonly userrRepository: Repository<User>,
        @InjectRepository(Company)
        private readonly companyRepository: Repository<Company>,
        @InjectRepository(Status)
        private readonly statusRepository: Repository<Status>,
        @InjectRepository(Price)
        private readonly priceRepository: Repository<Price>,
        @InjectRepository(Funds)
        private readonly fundsRepository: Repository<Funds>,
        private readonly cache: CachingService,
    ) { }

    async createOrder(clientId: string, _order: OrderDTO) {
      try {
        console.log(_order);
        let client = await this.userrRepository.findOne({ id: clientId });
        const company = await this.companyRepository.findOne({ id: _order.companyId });
        const price = await this.priceRepository.findOne({ where: { company }, order: { opendate: 'DESC' } });
        const total = price.endprice * _order.units;
        if (total > client.funds.currentamount) {
            throw new HttpException('Insufficient funds!', HttpStatus.NOT_FOUND);
        }
        client.funds.currentamount -= total;
        client = await this.userrRepository.save(client);

        const funds = client.funds;

        funds.currentamount -= total;

        this.fundsRepository.save(funds);

        const order = new Order();
        order.buyprice = price.endprice;
        order.sellprice = 0;
        order.opendate = new Date();
        order.client = Promise.resolve(client);
        order.company = company;
        order.units = _order.units;
        order.status = await this.statusRepository.findOne({ where: { statusname: 'opened' } });

        const newOrder = await this.orderRepository.save(order);

        this.cache.addOrder(client, newOrder);

        return { message: 'Complete!' };
      } catch (e) {
        console.log(e);
      }
    }

    async getOrdersAll() {
        try {
            const foundOrders = await this.orderRepository.find();
            return foundOrders;
        } catch (error) {
            throw new HttpException('Open orders not found!', HttpStatus.NOT_FOUND);
        }
    }

    async getOrdersByClient(client: User) {
        const foundOrder = await this.orderRepository.find({ where: { client } });

        if (!foundOrder) {
            throw new HttpException('Orders not found!', HttpStatus.NOT_FOUND);
        }

        return foundOrder;
    }

    async closeOrder(id: string): Promise<any> {
        try {
            const order: Order = await this.orderRepository.findOne({ id });
            const client = await order.client;
            let funds = await this.fundsRepository.findOne({where: { client }});
            console.log(funds);
            const company = order.company;
            const price = await this.priceRepository.findOne({ where: { company }, order: { opendate: 'DESC' } });
            const total = price.endprice * order.units;
            order.closedate = new Date();
            order.sellprice = price.endprice;
            order.status = await this.statusRepository.findOne({ where: { statusname: 'closed' } });

            funds.currentamount += order.sellprice * order.units;

            funds = await this.fundsRepository.save(funds);
            console.log(funds);
            const newOrder = await this.orderRepository.save(order);

            this.cache.updateOrder(client, newOrder);

            return { message: 'Closed' };
        } catch (error) {
            throw new HttpException('Orders not found!', HttpStatus.NOT_FOUND);
        }

    }

    async getClosedOrders(id: string) {
        const foundStatus = await this.statusRepository.findOne({ where: { statusname: 'closed' } });
        const foundClosedOrders = await this.orderRepository.find({
            where: {
                clientId: id,
                status: foundStatus.id,
            },
            order: {
                closedate: 'DESC',
            },
        });

        if (!foundClosedOrders || foundClosedOrders.length === 0) {
            return [];
        }
        return foundClosedOrders;
    }

    async getOpenOrders(id: string) {

        const foundStatus = await this.statusRepository.findOne({ where: { statusname: 'opened' } });
        const foundOpenOrders = await this.orderRepository.find({
            where: {
                clientId: id,
                status: foundStatus.id,
            },
        });

        if (!foundOpenOrders) {
            throw new HttpException('Open orders not found!', HttpStatus.NOT_FOUND);
        }
        if (foundOpenOrders.length === 0) {
            throw new BadRequestException('Client has no opened orders');
        }

        return foundOpenOrders;
    }

    async getOrdersInInterval(start: Date, end: Date) {
        const foundOrdersInInrerval = await this.orderRepository.find({
            where: {
                opendate: start,
                closedate: end,
            },
        });

        if (!foundOrdersInInrerval) {
            throw new HttpException('Orders in set interval not found!', HttpStatus.NOT_FOUND);
        }

        return foundOrdersInInrerval;
    }
}
