import { Injectable } from '@nestjs/common';
import { Company } from 'src/data/entities/company.entity';
import { Order } from 'src/data/entities/order.entity';
import { User } from 'src/data/entities/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Price } from 'src/data/entities/prices.entity';

@Injectable()
export class CachingService {

  public loaded = false;

  public companies: Company[] = [];
  public users: User[] = [];
  public orders: Map<string, Order[]> = new Map<string, Order[]>();
  public partials: Map<string, number[]> = new Map<string, number[]>();
  public clients: Map<string, string[]> = new Map<string, string[]>();
  public prices: Price[] = [];

  public constructor(
    @InjectRepository(Price) private readonly priceRepository: Repository<Price>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Company) private readonly companyRepository: Repository<Company>,
    @InjectRepository(Order) private readonly ordersRepository: Repository<Order>,
  ) {
    this.init().then(() => console.log(`cache initialized`)).then(() => this.loaded = true).catch(console.log);
  }

  private async init(): Promise<void> {
    // Initialize users and orders
    const users = await this.userRepository.find({});

    for (const user of users) {
      await this.addUser(user);
      if (user.role.rolename === 'client') {
        const orders = await this.ordersRepository.find({where: { client: user }});
        if (orders) {
          this.orders.set(user.id, orders);
        } else {
          this.orders.set(user.id, []);
        }
      }
    }

    // Initialize companies
    this.companies = await this.companyRepository.find({});

    // Initialize partials
    const prices = [];

    for (const company of this.companies) {
      const price = await this.priceRepository.findOne({where: { company }, order: { opendate: 'DESC' }});
      prices.push(price);
      this.partials.set(company.id, [price.endprice]);
    }

    this.prices = prices;
  }

  public getOrders(client: User): Order[] {
    return this.orders.get(client.id);
  }

  public updatePrice(price): void {
    const _price = this.prices.find((priceEl: Price) => priceEl.id === price.id);
    if (_price) {
      Object.keys(price).forEach((key: string) => _price[key] = price[key]);
    }
  }

  public async addOrder(client: User, order: Order): Promise<void> {
    const orders = this.orders.get(client.id);
    if (!orders) {
      this.orders.set(client.id, [order]);
      return;
    }

    const _order = orders.find((orderEl: Order) => order.id === orderEl.id);

    if (!_order) {
      orders.push(order);
      this.orders.set(client.id, orders);
    }

    console.log(this.orders.get(client.id));
  }

  public async updateOrder(client: User, order: Order): Promise<void> {
    const orders = this.orders.get(client.id);

    const _order = orders.find((orderEl: Order) => orderEl.id === order.id);

    if (_order) {
      Object.keys(order).forEach((key: string) => _order[key] = order[key]);
      this.orders.set(client.id, orders);
    }

    console.log(this.orders.get(client.id));
  }

  public async addUser(user: User): Promise<void> {
    const _user = this.users.find((userEl: User) => userEl.id === user.id);

    if (!_user) {
      this.users.push(user);
      if (user.role.rolename === 'client') {
        this.orders.set(user.id, []);
      }
      // update manager : client[] relationship
      const manager = await user.manager;
      if (manager) {
        if (!this.clients.get(manager.id)) {
          this.clients.set(manager.id, [user.id]);
        } else {
          const clients = this.clients.get(manager.id);
          clients.push(user.id);
          this.clients.set(manager.id, clients);
        }
      }
    }
  }

  public async updateUser(user: User): Promise<void> {
    const _user = this.users.find((userEl: User) => userEl.id === user.id);

    if (_user) {
      Object.keys(_user).forEach((key: string) => _user[key] = user[key]);
    }
  }

}