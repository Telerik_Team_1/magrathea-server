import { Company } from '../../../data/entities/company.entity';
import { Injectable, HttpStatus, HttpException } from '@nestjs/common';
import { User } from '../../../data/entities/user.entity';
import { Repository, AdvancedConsoleLogger } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { watch } from 'fs';
import { Watchlist } from 'src/data/entities/watchlist.entity';
import { CachingService } from './caching.service';
@Injectable()
export class WatchlistService {

    private currentWatchlist: Watchlist;

    constructor(
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,

    @InjectRepository(Watchlist)
    private readonly watchlistRepository: Repository<Watchlist>,

    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
    private readonly cache: CachingService,
    )

    {}

    async getCompanies(clientID: string): Promise<Company[]> {

    const client = await this.usersRepository.findOne({id: clientID});
    if (!client){
            throw new HttpException('There is no such user', HttpStatus.NOT_FOUND);
    }

    return await client.companies || [];

    const companies = await this.companyRepository.find({where: { users: client }});
    console.log(companies);

    return companies || [];

    console.log(`get watlist`);
    console.log(client);

    const watchlist = await this.watchlistRepository.findOne( { where: { client }});
    console.log(watchlist);

    if (!watchlist){
        return [];
    }
    // const _client = await watchlist.client;

    // console.log(client.id, _client.id);

    // return watchlist.companies;
    }

    async addCompany(clientId: string, companyId: string): Promise<object> {
        try {
            console.log(clientId, companyId);
            const client = await this.usersRepository.findOne({ id: clientId });
            const company = await this.companyRepository.findOne({ id: companyId });
            const watchlist = await client.companies || [];

            const contained = watchlist.find((_company: Company) => _company.id === company.id);

            if (!contained) {
                watchlist.push(company);
            }

            client.companies = Promise.resolve(watchlist);

            await this.usersRepository.save(client);

            return { message: 'Added' };

        } catch (error) {
            console.log(`error details: Error on method addCompany\n`);
            console.log(`error message: ${error}`);
            throw new HttpException('Cannot add company', HttpStatus.BAD_REQUEST);
        }

    }

    // async removeCompany(companyId: string): Promise<object>{
    //     try {
    //         const watchlistCompanies = await this.currentWatchlist.companies;
    //         const initialNumberOfCompanies = watchlistCompanies.length;

    //         for (const [index, company] of watchlistCompanies.entries()) {
    //             if (company.id === companyId) {
    //                 watchlistCompanies.splice(index, 1);
    //                 break;
    //             }
    //         }

    //         if (initialNumberOfCompanies === watchlistCompanies.length){
    //             throw new HttpException('Company not found in watchlist', HttpStatus.NOT_FOUND);
    //         }
    //         // updating watchlist in service also in db
    //         await this.watchlistRepository.save(this.currentWatchlist);

    //         return { result: `Company with id:${companyId} has been removed from watchlist!`};
    //     } catch (error) {
    //         console.log(`error details: Error on method removeCompany\n`);
    //         console.log(`error message: ${error}`);
    //         throw new HttpException('Cannot remove company', HttpStatus.BAD_REQUEST);
    //     }
    // }
}