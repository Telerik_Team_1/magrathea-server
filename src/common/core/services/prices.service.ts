import { Injectable, HttpException, HttpStatus, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Between, MoreThan, In } from 'typeorm';
import { Price } from 'src/data/entities/prices.entity';
import { Company } from 'src/data/entities/company.entity';
import { CachingService } from './caching.service';

@Injectable()
export class PricesService {

  constructor(
    @InjectRepository(Price)
    private readonly priceRepository: Repository<Price>,
    @InjectRepository(Company)
    private readonly companyRepository: Repository<Company>,
    private readonly cache: CachingService,
) { }

  async getCompanyPrices(id: string, lastN: number, startdate?: Date, enddate?: Date, type?: string): Promise<any[]> {
    const company = await this.companyRepository.findOne({ id });
    if (!company) {
      throw new HttpException('Company doesn\'t exist!', HttpStatus.NOT_FOUND);
    }

    if (type && type === 'day') {
      const _price = this.cache.prices.find((xprice: Price) => xprice.company.id === company.id);
      // console.log(_price);
      const dates = [];
      for (let i = 0; i < 24; i++) {
        const date = new Date(_price.opendate.valueOf() - i * 60 * 60 * 1000);
        dates.push(date);
      }

      const prices = await this.priceRepository.find({where: {opendate: In(dates)}, order: { opendate: 'DESC' }});
      return prices.filter((xprice: Price) => xprice.company.id === company.id).map((xprice: Price) => xprice.endprice).slice(0, 24);

      const data = await this.priceRepository.find({ where: { company }, order: { opendate: 'DESC' }, take: 24 * 60 * 60 + 1 });
      const result = [];
      for (let i = 0; i < 20; i++) {
        const price = data[i * 24 * 60];
        if (price) {
          result.push(price.endprice);
        }
      }
      return result;
    }

    if (lastN) {
      return await this.priceRepository.find({ where: { company }, order: { opendate: 'DESC' }, take: lastN});
    }
    if (startdate && enddate) {
      return await this.priceRepository.find({ opendate: Between (startdate, enddate), company });
    }
    if (startdate && !enddate) {
      return await this.priceRepository.find({ opendate: MoreThan (startdate.valueOf() - 1), company });
    }

    return [await this.priceRepository.findOne({ where: { company }, order: { opendate: 'DESC' } })];
  }

  async getLastPricePerCompany(): Promise<Price[]> {
    const companies = await this.companyRepository.find({});
    const result = [];

    for (const company of companies) {
      try {
        const price = await this.priceRepository.findOne({ where: { company }, order: { opendate: 'DESC' } });
        result.push(price);
      } catch (e) {
        // Log error if necessary
      }
    }

    return result;
  }

}