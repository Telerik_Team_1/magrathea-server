
import { Controller, Post, Body, Get, UseGuards, Param } from '@nestjs/common';
import { IndustriesService } from 'src/common/core/services/industry.service';
import { Industry } from 'src/data/entities/industry.entity';
import { IndustryDTO } from 'src/models/industry/industry.dto';
import { IndustryUpdateDTO } from 'src/models/industry/industryUpdate.dto';
import { Roles, RolesGuard } from 'src/common';
import { AuthGuard } from '@nestjs/passport';
import { OrderService } from 'src/common/core/services/order.service';

@Controller('orders')
export class OrdersController {
  constructor(private readonly ordersService: OrderService) {}
  @Get('')
  @Roles('manager')
  @UseGuards(AuthGuard(), RolesGuard)
  async getNothing(): Promise<any> {
    return { message: 'nothing'};
  }
  @Get('history/:id')
  @Roles('manager')
  @UseGuards(AuthGuard(), RolesGuard)
  async getHistory(@Param('id') id: string): Promise<any> {
    console.log(`history`);
    return await this.ordersService.getClosedOrders(id);
  }
  @Post('buy')
  @Roles('manager')
  @UseGuards(AuthGuard(), RolesGuard)
  async buy(@Body() data: any): Promise<any> {
    return await this.ordersService.createOrder(data.clientId, data.order);
  }

  @Post('sell')
  @Roles('manager')
  @UseGuards(AuthGuard(), RolesGuard)
  async sell(@Body() data: any): Promise<any> {
    return await this.ordersService.closeOrder(data.orderId);
  }
}
