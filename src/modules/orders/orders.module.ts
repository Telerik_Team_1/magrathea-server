import { Module } from '@nestjs/common';
import { CoreModule } from 'src/common/core/core.module';
import { AuthModule } from 'src/auth/auth.module';
import { OrdersController } from './orders.controler';
import { IndustriesService } from 'src/common/core/services/industry.service';
import { OrderService } from 'src/common/core/services/order.service';

@Module({
  imports: [CoreModule, AuthModule],
  controllers: [OrdersController],
  providers: [OrderService],
})
export class OrdersModule {}