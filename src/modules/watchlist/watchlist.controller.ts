import { User } from '../../data/entities/user.entity';
import { Company } from '../../data/entities/company.entity';
import { WatchlistService } from '../../common/core/services/watchlist.service';
import { Controller, Get, UseGuards, Param, Body, Post } from '@nestjs/common';
import { Roles, RolesGuard } from 'src/common';
import { AuthGuard } from '@nestjs/passport';

@Controller()
export class WatchlistController {

    constructor(private readonly watchlistService: WatchlistService) {}

    @Get('watchlist/:id')
    @Roles('manager')
    @UseGuards(AuthGuard(), RolesGuard)
    async getWatchlist(@Param('id') id: string): Promise<Company[]> {
        return await this.watchlistService.getCompanies(id);
    }

    @Post('watchlist/addcompany')
    @Roles('manager')
    @UseGuards(AuthGuard(), RolesGuard)
    async addCompany(@Body() body: any): Promise<object> {
        return await this.watchlistService.addCompany(body.clientId, body.companyId);
    }

    @Post('watchlist/removecompany')
    @Roles('manager')
    @UseGuards(AuthGuard(), RolesGuard)
    async removeCompany(): Promise<object> {
        // for testing purposes - this will be taken from the body
        const company1 = new Company();
        company1.id = 'newcompanyid';
        company1.name = 'newcompanyname';
        company1.closedate = new Date();
        //
        return await this.watchlistService.removeCompany(company1.id);
    }

}
