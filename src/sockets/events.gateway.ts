import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  WsResponse,
} from '@nestjs/websockets';
import { AuthService } from 'src/auth/auth.service';
import * as WebSocket from 'ws';
import { GetUserDTO } from 'src/models/user/get-user.dto';
import { JwtService } from '@nestjs/jwt';
import { SocketedUser } from 'src/models/socketed-user';
import { UpdaterService } from './services/updater.service';
import { ClientRecord } from 'src/models/client-record';
import { Price } from 'src/data/entities/prices.entity';
import { CachingService } from 'src/common/core/services/caching.service';

@WebSocketGateway(8081)
export class EventsGateway {

  private running = true;
  private lastRun: Date = new Date();
  private data = [1, 2, 3];
  private clients: SocketedUser[] = [];

  @WebSocketServer() server;

  public constructor(
    private readonly authService: AuthService,
    private readonly jwt: JwtService,
    private readonly updater: UpdaterService,
    private readonly cache: CachingService,
  ) {
    console.log(`initializing update loop...`);
    this.update().then(() => console.log('init update'));
  }

  @SubscribeMessage('mount')
  async mount(socket: WebSocket, token: string): Promise<string> {
    let payload;
    try {
      payload = this.jwt.verify(token);
    } catch (e) {
      payload = '';
    }
    const user: GetUserDTO = await this.authService.validateUser(payload);
    if (!user) {
      return 'Invalid credentials!';
    }
    if (this.clients.indexOf(socket) < 0) {
      this.clients.push({
        socket,
        user,
        token,
      });
    }
    socket.onclose = () => {
      this.clients.filter((_) => _.socket !== socket);
    };
    return 'mounted';
  }

  @SubscribeMessage('unmount')
  async unmount(socket, token): Promise<string> {
    this.clients = this.clients.filter((socketedUser: SocketedUser) => socketedUser.token !== token);
    return 'unmounted';
  }

  private async update(): Promise<any> {
    if (this.cache.loaded) {
      const data = await this.updater.update();
      this.clients = this.clients.filter((socketedUser: WebSocket) => socketedUser.socket.readyState === socketedUser.socket.OPEN);
      this.clients.forEach((socketedUser: WebSocket) => {
        const clientData = data.clientData.find((clientRec: ClientRecord) => clientRec.manager === socketedUser.user.id );
        const returnData = { companies: data.companies, clients: clientData.clients, prices: data.prices.map((price: any) => {
          const _price = JSON.parse(JSON.stringify(price));
          _price.sell = price.endprice;
          _price.buy = price.endprice * 1.0015;
          return _price;
        }) };
        socketedUser.socket.send(JSON.stringify(returnData));
        console.log(`emiting socket data`);
      });
    }
    const prev = this.lastRun.valueOf();
    this.lastRun = new Date();
    const delta = this.lastRun.valueOf() - prev;
    const nextRunTime = 1000 - delta > 0 ? 1000 - delta : 0;
    if (this.running) {
      setTimeout(() => this.update().then(() => {console.log(`delta: ${delta}`); }), 950);
    }
  }
}