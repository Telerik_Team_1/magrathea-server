import { Module } from '@nestjs/common';
import { EventsGateway } from './events.gateway';
import { CoreModule } from 'src/common/core/core.module';
import { AuthModule } from 'src/auth/auth.module';
import { UpdaterService } from './services/updater.service';

@Module({
  imports: [CoreModule, AuthModule],
  providers: [EventsGateway, UpdaterService],
})
export class EventsModule {}