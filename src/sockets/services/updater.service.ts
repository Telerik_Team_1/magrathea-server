import { Injectable } from '@nestjs/common';
import { CompaniesService } from 'src/common/core/services/companies.service';
import { FundsService } from 'src/common/core/services/funds.service';
import { OrderService } from 'src/common/core/services/order.service';
import { UsersService } from 'src/common/core/services/users.service';
import { Company } from 'src/data/entities/company.entity';
import { Price } from 'src/data/entities/prices.entity';
import { PricePartial } from 'src/models/price-partial';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { SETUP } from 'src/setup/settings';
import { CompanyRecord } from 'src/models/company-record';
import { ClientRecord } from 'src/models/client-record';
import { User } from 'src/data/entities/user.entity';
import { PricesService } from 'src/common/core/services/prices.service';
import { CachingService } from 'src/common/core/services/caching.service';
import { runInThisContext } from 'vm';

@Injectable()
export class UpdaterService {

  private companies: Company[] = [];
  private prices: Price[] = [];
  public partials: PricePartial[] = [];
  private clientData: ClientRecord[] = [];
  private lastEmitedFull: Date = null;
  private users: User[] = [];

  constructor(
    private readonly companiesService: CompaniesService,
    private readonly fundsService: FundsService,
    private readonly orderService: OrderService,
    private readonly usersService: UsersService,
    private readonly priceService: PricesService,
    private readonly cache: CachingService,
    @InjectRepository(Price)
    private readonly priceRepository: Repository<Price>,
  ) {
  }

  public async update() {
    // Initialize collections, update and sync data
    if (!this.companies.length) {
      this.companies = await this.companiesService.getAll();
      // generate full db records up to this moment
      const records = await this.regenerate(this.companies);
      const xnow = new Date();
      const subtics = Math.floor((xnow.valueOf() - this.lastEmitedFull.valueOf()) / 1000);
      // generate tick data per seconds up to this moment
      for (const company of this.companies) {
        const priceRecord = records.find((record: CompanyRecord) => record.company === company.name);
        let price = priceRecord.price;
        const gapTickDeltas = this.generateData(subtics, price, true).slice(1);
        const gapTickData = [price];
        gapTickDeltas.forEach((delta: number) => {
          price += Math.random() > 0.5 ? delta : -delta;
          gapTickData.push(+price.toFixed(2));
        });
        this.partials.push({companyname: company.name, prices: gapTickData});
      }
    }

    const now = new Date();
    // per tick calculations
    for (const partial of this.partials) {
      const lastTickPrice = partial.prices[partial.prices.length - 1];
      const delta = this.generateData(1, lastTickPrice, true)[1];
      const nextLastPrice = Math.abs(Math.random() > 0.5 ? lastTickPrice + delta : lastTickPrice - delta);
      const updatedPrice = this.prices.find((price: Price) => price.company.name === partial.companyname);
      partial.prices.push(+nextLastPrice.toFixed(2));
      if (now.valueOf() - this.lastEmitedFull.valueOf() > 60 * 1000) {
        const price = new Price();
        price.startprice = partial.prices[0];
        price.endprice = Math.abs(partial.prices[partial.prices.length - 1]);
        price.lowprice = Math.abs(Math.min(...partial.prices));
        price.highprice = Math.abs(Math.max(...partial.prices));
        price.opendate = this.lastEmitedFull;
        price.company = this.companies.find((company: Company) => company.name === partial.companyname);
        partial.prices = [price.endprice];
        console.log(`full update on ${partial.companyname}`);
        const newPrice = await this.priceRepository.save(price);
        this.cache.updatePrice(newPrice);
      }
    }
    if (now.valueOf() - this.lastEmitedFull.valueOf() > 60 * 1000) {
      this.lastEmitedFull = new Date(this.lastEmitedFull.valueOf() +  60 * 1000);
    }

    await this.getClientDataByManager();
    const prices = this.cache.prices;

    prices.forEach((price: Price) => {
      const partial = this.partials.find((_: PricePartial) => _.companyname === price.company.name);
      price.endprice = Math.abs(partial.prices[partial.prices.length - 1]);
    });

    return {
      companies: this.companies,
      clientData: this.clientData,
      prices,
    };
  }

  private async getClientDataByManager(): Promise<void> {
    let users = this.users;
    if (!users.length) {
      users = await this.usersService.getAll();
    }
    const clientData: ClientRecord[] = [];
    for (const user of users) {
      if (user.role.rolename === 'manager') {
        const cd: any = {};
        cd.manager = user.id;
        cd.clients = [];
        for (const _user of users) {
          if (_user.manager) {
            const manager = await _user.manager;
            if (manager && manager.id === user.id) {
              const updated_user = {
                id: _user.id,
                email: _user.email,
                funds: _user.funds.currentamount,
                fullname: _user.fullname,
                dateRegistered: _user.dateregistered,
                orders: this.cache.getOrders(_user),
                watchlist: [],
              };
              cd.clients.push(updated_user);
            }
          }
        }
        clientData.push(cd);
      }
    }
    this.clientData = clientData;
  }

  private async regenerate(companies: Company[]): Promise<CompanyRecord[]> {
    const records: CompanyRecord[] = [];
    for (const company of companies) {
      try {
        // Find the most recent record
        const price = await this.priceRepository.findOne({ where: { company }, order: { opendate: 'DESC' } });

        // Setup the start date for the update to the most recent record if any, else the initial start date
        const startdate = price.opendate;
        this.lastEmitedFull = price.opendate;

        // Calculate the number of records of fake data needed to bring the mock to the current date
        const now = new Date();
        const loops = Math.floor((now.valueOf() - startdate.valueOf()) / (1000 * SETUP.tick));

        const prices: Price[] = [];
        let lastRecord = price.endprice;

        if (loops) {
          console.log(`regenerating ${loops} records...`);
          // Generate fake data
          const stockMockData = this.generateData(loops, price ? price.endprice : undefined);
          for (let i = 0; i < stockMockData.length - 1; i++) {
            const newPrice = new Price();
            newPrice.opendate = new Date(startdate.valueOf() + (i + 1) * SETUP.tick * 1000);
            newPrice.startprice = stockMockData[i];
            newPrice.endprice = stockMockData[i + 1];
            const delta = Math.abs(stockMockData[i + 1] - stockMockData[i]);
            const min = Math.min(stockMockData[i], stockMockData[i + 1]);
            const highlow = [min + Math.random() * delta, min + Math.random() * delta];
            newPrice.lowprice = +Math.min(...highlow).toFixed(2);
            newPrice.highprice = +Math.max(...highlow).toFixed(2);
            newPrice.company = company;
            prices.push(newPrice);
            this.lastEmitedFull = newPrice.opendate;
            lastRecord = newPrice.endprice;
          }
          // Try to update the stocks
          try {
            await this.priceRepository.save(prices);
            console.log(`Update market data for ${company.name} with ${loops} records created from ${startdate.toDateString()}`);
          } catch (e) {
            console.warn(e);
          }
        } else {

          console.log(`Market data for ${company.name} is up to date.`);
        }
        records.push({company: company.name, price: lastRecord});

      } catch (error) {
        console.log(`oopsie... market update failed, reason: ${error.message}`);
      }
    }
    return records;
  }

  private generateData = (loops: number, value?: number, seconds = false): number[] => {
    const min = 10;
    const max = 500;
    const volatility = 1.0 + Math.random() * 1.5;
    value = value || 200 + Math.random() * 200;

    const stocks = [value];

    for (let i = 0; i < loops; i++) {
      let mod = 0.5;
      if (value > max) mod = 0.8;
      if (value < min) mod = 0.2;
      const dir = (Math.random() >= mod) ? 1 : -1;

      // maxToAdd = max amount that the value can change
      const maxToAdd = (max - min) * volatility / 100;
      value += (Math.random() * maxToAdd * dir);

      // value can't be 0 -- random number from 0 to 10
      if (value < 0) value = Math.random() * 10;

      if (seconds) {
        value /= 60;
      }

      // rounds to 2 decimal places
      value = (Math.round(100 * value)) / 100;
      stocks.push(Math.abs(value));
    }

    return stocks;
  }

}