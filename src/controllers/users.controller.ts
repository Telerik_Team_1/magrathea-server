import { AdminGuard } from './../common/guards/roles/admin.guard';
import { AuthGuard } from '@nestjs/passport';
import { Request, Controller, Get, UseGuards, Post, Body, Param, Put } from '@nestjs/common';
import { UsersService } from '../common/core/services/users.service';
import { User } from 'src/data/entities/user.entity';
import { Roles } from 'src/common/decorators/roles.decorator';
import { RegisterDTO } from 'src/models/user/register.dto';
import { Settings } from 'src/data/entities/settings.entity';
import { RolesGuard } from 'src/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Funds } from 'src/data/entities/funds.entity';

@Controller('users')
export class UsersController {

  constructor(
    private readonly usersService: UsersService,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(Funds) private readonly fundsRepository: Repository<Funds>,
    ) { }

    @Get()
    @Roles('admin')
    @UseGuards(AuthGuard(), AdminGuard)
    async all() {
      const users = await this.usersService.getAll();
      return users.map((user: User) => {
        return {
          id: user.id,
          name: user.fullname,
          email: user.email,
          role: user.role.rolename,
        };
      });
    }

    @Post()
    @Roles('admin')
    @UseGuards(AuthGuard(), AdminGuard)
    async createUser(@Body() user: Partial<RegisterDTO>) {
      return await this.usersService.createUser(user);
    }

    @Post('/:id')
    @Roles('admin', 'manager')
    @UseGuards(AuthGuard(), RolesGuard)
    async updateUser(
      @Body() user: Partial<RegisterDTO>,
      @Param('id') id: string,
      ) {
        console.log('updating user');
        return await this.usersService.updateUser(user, id);
    }

    @Put('/funds')
    @Roles('manager')
    @UseGuards(AuthGuard(), RolesGuard)
    async updateFonds(
      @Body() body: any,
      ) {
        console.log('updating funds');
        const user = await this.userRepository.findOne({ id: body.id });
        const funds = await this.fundsRepository.findOne({ where: { client: user } });
        if (body.add) {
          funds.currentamount += body.amount;
        } else {
          if (body.amount <= funds.currentamount) {
            funds.currentamount -= body.amount;
          }
        }

        await this.fundsRepository.save(funds);

        return { message: 'Success!' };
    }

    @Get('/managers')
    @Roles('admin')
    @UseGuards(AuthGuard(), AdminGuard)
    async getAllManagers() {
      const managers = await this.usersService.getAllManagers();
      return managers.map((user: User) => {
        return {
          name: user.fullname,
          email: user.email,
        };
    });
  }

  @Get('/:id') // important - define after '/***', else '***' taken for id param
  @Roles('admin')
  @UseGuards(AuthGuard(), AdminGuard)
  async getUserById(
    @Param('id') id: string,
  ) {
    return await this.usersService.getUserById(id);
  }
  // @Get(':id/clients')
  // @UseGuards(AuthGuard())
  // async getCustomers(
    //   @Request() req: any,
  // ): Promise<User[]> {

  //   const user: User = req.user;
  //   const clients: User[] = await Promise.resolve(user.clients);

  //   clients.forEach(async (client) => {
  //     client.orders = Promise.resolve(client.orders);
  //   });

  //   return clients;
  // }

}
